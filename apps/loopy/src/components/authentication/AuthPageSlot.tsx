import { Box, Grid, Paper, Typography } from '@mui/material';
import { Outlet } from 'react-router';
import Logo from '../../assets/logo_1.png';
import { useLocation } from 'react-router';
import { injectIntl, IntlShape } from 'react-intl';

export function AuthPageSlot({ intl: { formatMessage } }: { intl: IntlShape }) {
  const location = useLocation();
  return (
    <Box
      sx={{
        height: '100vh',
        width: '100vw',
        backgroundImage: "url('../../assets/auth_bg.png')",
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
      }}
    >
      <Grid
        container
        justifyContent="center"
        alignContent="center"
        sx={{ height: '100%' }}
      >
        <Grid item xs={8} md={6} lg={4} sx={{ height: '90%' }}>
          <Paper
            elevation={2}
            sx={{
              height: '100%',
              display: 'grid',
              gridTemplateRows: 'auto auto 1fr',
            }}
          >
            <img
              src={Logo}
              alt="loopy logo"
              style={{
                height: 100,
                width: 100,
                marginTop: 28,
                justifySelf: 'center',
              }}
            />
            <Typography
              variant="h3"
              sx={{ justifySelf: 'center', marginTop: '26px' }}
            >
              {formatMessage({ id: location.pathname })}
            </Typography>
            <Outlet></Outlet>
          </Paper>
        </Grid>
      </Grid>
    </Box>
  );
}
export default injectIntl(AuthPageSlot);
