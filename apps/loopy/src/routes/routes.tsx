import { Navigate } from 'react-router';
import AuthPageSlot from '../components/authentication/AuthPageSlot';
import Signin from '../components/authentication/Signin';

export const routes = [
  {
    path: '/',
    element: <AuthPageSlot />,
    children: [{ path: '/sign-in', element: <Signin /> }],
  },
  { path: '*', element: <Navigate to="/" /> },
];
